# Open Fusion LED Controller Project

## This is the Main repository for the project

Documentation and diagrams.

Please see the sub folders.

### Links to other repositories

* Main repository for documentation and diagrams: [open-fusion-led-controller-main](https://bitbucket.org/2E0PGS/open-fusion-led-controller-main)
* Core Arduino repository: [open-fusion-led-controller-arduino](https://bitbucket.org/2E0PGS/open-fusion-led-controller-arduino)
* Web Arduino repository: [open-fusion-led-controller-web-arduino](https://bitbucket.org/2E0PGS/open-fusion-led-controller-web-arduino)
* Raspberry Pi repository: [open-fusion-led-controller-raspberrypi](https://bitbucket.org/2E0PGS/open-fusion-led-controller-raspberrypi)
* Windows remote repository: [open-fusion-led-controller-win-remote](https://bitbucket.org/2E0PGS/open-fusion-led-controller-win-remote)

### About

* This project is a multifunction LED strip light controller based on Arduino.
* The Arduino handles all the real time electronics such as PWM etc. 
* The Raspberry Pi handles all the web stuff such as serving the webpage with controls and parsing the requests.
* The Arduino and Raspberry Pi are linked via USB serial. 

### Features

* Auto LDR dimming
* Fade modes
* Jump R,G,B modes
* Ethernet control with API and webpage
* Speed control of effects
* Brightness control
* Custom colour picker
* Smart modes
* Support for clever and dumb LED strips

### Compatibility

* Works with (Dumb) 5050 RGB LED strips.
* Works with WS2812 (Smart) (Addressable) RGB LED strips.

### Licence

```
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
