# Raspberry PI Python Flask web API

Please read the USB serial API documentation for more detailed information.

The web API is simply a minimal proxy for information which then gets sent via USB serial.

## Intro

* The web API uses the Python Flask library.
* By default the web server runs at port 5000.
* Debug mode is enabled by default.
* Commands are sent via the query string in the GET requests.
* Format for commands are: `?<command>=<value1>, <value2>, <value3>`
* Example: `?setoutput=1`
* Multi value example: `?setcustom=100, 100, 0`


## Unquoted query params

* If commas are used in query strings they are encapsulated by your browser.
* Example: `?setcustom=100,%20100,%200`
* Don't worry as the Python web API can handle these.

