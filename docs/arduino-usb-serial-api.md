# Arduino USB serial API

## Intro

* USB serial runs at 9600 baud.
* End of line terminator is a newline character `\n`
* Format for commands are: `<command>, <value1>, <value2>, <value3>`
* Values 2 and 3 are only used for certain commands as explained in further reading.

## Setting power status of lights

* Format: `setpower, <status>`
* Example: `setpower, 1`
* 0 is OFF and 1 is on.
* Status must be a bool.

## Setting the output type

* Format: `setoutput, <output>`
* Example: `setoutput, 1`
* 0 is dumb 5050 strips and 1 is SMART 5050 NeoPixels.
* Output must be an int.

## Setting the mode

* Format: `setmode, <mode>`
* Example: `setmode, 12`
* Modes must be an int.
* See modes section for avaiable modes.

## Modes

They are listed as follows: <name>, <id>, <description>

* AutoLDR, 1, Sets the lights equal to value read by the LDR (Light Dependant Resistor). Has a low light mode designed for late night lighting.
* 

## Setting the effect speed/delay

* Format: `settimer, <seconds>`
* Example: `settimer, 200`
* Seconds must be an int.

## Setting the debug level

This sets the console output debug verbosity level.

* Format: `setdebug, <level>`
* Example: `setdebug, 2`
* The level must be an int.

## Setting the global brightness

* Format: `setbrightness, <level>`
* Example: `setbrightness, 100`
* The brightness level must be a int value from 0 - 255.

## Setting custom colour vales

* Format: `setcustom, <red-level>, <green-level>, <blue-level>`
* Example: `setcustom, 100, 100, 0`
* This gives us a yellow colour.
* The RGB colour levels must each be an int which ranges from 0 - 255.
